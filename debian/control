Source: libbigwig
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Steffen Moeller <moeller@debian.org>
Section: science
Priority: optional
Build-Depends: dpkg-dev (>= 1.22.5), debhelper-compat (= 13),
               d-shlibs (>= 0.106~),
               libcurl4-gnutls-dev|libcurl-dev,
               zlib1g-dev
Build-Depends-Indep: doxygen
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/med-team/libbigwig
Vcs-Git: https://salsa.debian.org/med-team/libbigwig.git
Homepage: https://github.com/dpryan79/libBigWig/
Rules-Requires-Root: no

Package: libbigwig0t64
Provides: ${t64:Provides}
Replaces: libbigwig0
Conflicts: libbigwig0 (<< ${source:Version})
Architecture: any
Section: libs
Multi-Arch: same
Depends: ${shlibs:Depends},
         ${misc:Depends}
Recommends: curl
Description: C library for handling bigWig files
 This package provides a C library for reading/parsing local and remote
 bigWig and bigBed files.  This library was refurbished to particularly
 suit to be wrapped by scripting languages by avoiding early bailouts
 in case of errors.

Package: libbigwig-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: libbigwig0t64 (= ${binary:Version}),
         ${misc:Depends}
Description: C library for handling bigWig files - header files
 This package provides the files needed to develop with the libBigWig
 C library for reading/parsing local and remote bigWig and bigBed files.

Package: libbigwig-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Multi-Arch: foreign
Description: C library for handling bigWig files - documentation
 This package provides the doxygen-created inline documentation needed
 to develop with the libBigWig C library for reading/parsing local and
 remote bigWig and bigBed files.
